﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p2
{
    class Program
    {
        static void Main(string[] args)
        {
            int functionality = 0, algorithm = 0; 
            const int MIN_OPTION = 1, MAX_OPTION_MAIN = 4, MAX_OPTION_ALGO = 5;
            //const string node = "nodes.json";
            //const string server = "servers_plus.json";
            //const string user = "users.json";

            string node = args[0];
            string server = args[1];
            string user = args[2];

            Menu m = new Menu();

            ArrayList arrNode = new ArrayList();
            ArrayList arrServer = new ArrayList();
            ArrayList arrUser = new ArrayList();

            ////Poner en un arrayList los Nodos
            arrNode = File.loadNodeFile(node);
            arrServer = File.loadServerFile(server);
            arrUser = File.loadUserFile(user);

            m.printHeader();

            do
            {
                do
                {
                    m.printMainMenu();
                    m.getOption(1);
                    if (!m.isCorrect(MIN_OPTION, MAX_OPTION_MAIN, 1)) { m.printErrOption(); }
                    else { functionality = m.option1; }
                } while (!m.isCorrect(MIN_OPTION, MAX_OPTION_MAIN, 1));

                if (!m.isExit())
                {
                    if (functionality != 3)
                    {
                        do
                        {
                            m.printAlgoMenu();
                            m.getOption(2);
                            if (!m.isCorrect(MIN_OPTION, MAX_OPTION_ALGO, 2)) { m.printErrOption(); }
                            else { algorithm = m.option2; }
                        } while (!m.isCorrect(MIN_OPTION, MAX_OPTION_ALGO, 2));
                    }
                        m.startProgram(functionality, algorithm, ref arrNode, ref arrServer, ref arrUser);
                }     
            } while (!m.isExit());

        }
    }
}
