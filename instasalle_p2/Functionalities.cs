﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace instasalle_p2
{
    class Functionalities
    {
        public static int tolerancia = 3; // tolerancia
        public static int numCridesRec = 0;
        public struct SolutionPathA
        {
            public ArrayList path;
            public int cost_total;
            public double reliability_total;
        }

        public struct DistributedUser
        {
            public Usuari user;
            public bool localitzat;
        }
        public struct Distribution
        {
            public Server s;
            public ArrayList users; // of DistributedUser
            public double activitat_total;

        }
        public struct SolutionD
        {
            public ArrayList distribucio; // of Distribution
            public double equitativitat;
            public int localitzacio; // nombre de persones que estiguin ben localitzades segons ubicació
            public int numUsersDistribuits;
            public int currServerIndex;
            public int currUserIndex;
        }

        public struct ChoosenChild
        {
            public int cost;
            public int id;
        }

        public static void printPath(SolutionPathA s)
        {
            Console.Write("\t");
            foreach (Node path in s.path)
            {
                Console.Write(path.id + " ");
            }
            Console.WriteLine();
        }

        public static void cleanSolution(ref SolutionPathA s, int value)
        {
            s.cost_total = value;
            s.reliability_total = 1;
            s.path.Clear();
        }

        public static void copySolution(SolutionPathA org, ref SolutionPathA dest)
        {
            dest.cost_total = org.cost_total;
            dest.reliability_total = org.reliability_total;
            dest.path = copyContents(org.path);
        }
        public static ArrayList copyContents(ArrayList a)
        {
            ArrayList clone = (ArrayList)a.Clone();
            return clone;
        }

        public static SolutionPathA[] backtrackingA(Node[] nodes, Server server_orig, Server server_dest)
        {
            SolutionPathA[] solution = new SolutionPathA[2];
            int i = 0;
            int num_origin_nodes = server_orig.reachable_from.Length;

            // Per al camí amb menor cost
            SolutionPathA solActual = new SolutionPathA();
            SolutionPathA solFinal = new SolutionPathA();
            SolutionPathA solExtra = new SolutionPathA(); // On guardem el best diguem

            // Per al camí amb millor fiabilitat
            SolutionPathA solActual_fiable = new SolutionPathA();
            SolutionPathA solFinal_fiable = new SolutionPathA();
            SolutionPathA solExtra_fiable = new SolutionPathA(); // On guardem el best diguem

            // S'hauria de comprovar diferents camins per cada node a on està connectat el servidor
            // ja que el servidor pot està accesible des de diferents nodes
            // Anirem comparant el camí per cada node de connexió al servidor i mirant quin és el millor 

            // COST
            // Inicialitzar les solucions
            solFinal.cost_total = int.MaxValue;
            solFinal.path = new ArrayList();
            solActual.path = new ArrayList();

            // suposant que els nodes del JSON estan ordenats per id
            solActual = backTracking_cost(nodes, nodes[server_orig.reachable_from[i++] - 1], server_dest, solActual, solFinal);
            copySolution(solActual, ref solFinal);
            copySolution(solActual, ref solExtra);

            while (i < num_origin_nodes)
            {
                // Abans de calcular una altra solució a partir d'un altre node de connexió al servidor, netejem-la
                cleanSolution(ref solActual, 0);
                cleanSolution(ref solFinal, int.MaxValue);

                // Calculem l'altra solució
                solActual = backTracking_cost(nodes, nodes[server_orig.reachable_from[i] - 1], server_dest, solActual, solFinal);

                // Comprovem si aquesta nova solució és millor que l'anterior
                if (solActual.cost_total < solExtra.cost_total)
                {
                    copySolution(solActual, ref solExtra);
                }
                i++;
            }

            // RELIABILITY
            // Inicialitzar les solucions i comptador
            i = 0;
            solFinal_fiable.reliability_total = 1;
            solFinal_fiable.path = new ArrayList();
            solActual_fiable.path = new ArrayList();

            // suposant que els nodes del JSON estan ordenats per id
            solActual_fiable = backTracking_fiable(nodes, nodes[server_orig.reachable_from[i++] - 1], server_dest, solActual_fiable, solFinal_fiable);
            copySolution(solActual_fiable, ref solFinal_fiable);
            copySolution(solActual_fiable, ref solExtra_fiable);

            while (i < num_origin_nodes)
            {
                // Abans de calcular una altra solució a partir d'un altre node de connexió al servidor, netejem-la
                cleanSolution(ref solActual_fiable, 0);
                cleanSolution(ref solFinal_fiable, int.MaxValue);

                // Calculem l'altra solució
                solActual_fiable = backTracking_fiable(nodes, nodes[server_orig.reachable_from[i] - 1], server_dest, solActual_fiable, solFinal_fiable);

                // Comprovem si aquesta nova solució és millor que l'anterior
                if (solActual_fiable.reliability_total > solExtra_fiable.reliability_total)
                {
                    copySolution(solActual_fiable, ref solExtra_fiable);
                }
                i++;
            }

            solution[0] = solExtra;
            solution[1] = solExtra_fiable;

            return solution;
        }

        public static bool dest_reached(Node curr, Server dest)
        {
            bool reached = false;
            int i = 0;
            int num_dest_nodes = dest.reachable_from.Length;
            while (!reached && i < num_dest_nodes)
            {
                if (curr.id == dest.reachable_from[i])
                {
                    reached = true;
                }
                i++;
            }
            return reached;
        }

        public static bool nodeInPath(Node curr, SolutionPathA s)
        {
            return s.path.Contains(curr);
        }

        public static SolutionPathA backTracking_cost(Node[] nodes, Node curr, Server dest, SolutionPathA s, SolutionPathA best)
        {
            int i;
            // cas trivial
            if (dest_reached(curr, dest))
            {
                if (s.cost_total < best.cost_total)
                {
                    s.path.Add(curr);
                    copySolution(s, ref best);
                }
            }
            else // cas no trivial
            {
                for (i = 0; i < curr.connectsTo.Length; i++)
                {
                    // Si trobem el node en l'array de connexions, mirem el següent
                    if (!nodeInPath(curr, s))
                    {
                        if (s.cost_total + curr.connectsTo[i].cost < best.cost_total)
                        {
                            // Afegim el node actual al camí de la solució i sumem la distància al nou node
                            s.path.Add(curr);
                            s.cost_total += curr.connectsTo[i].cost;
                            best = backTracking_cost(nodes, nodes[curr.connectsTo[i].to - 1], dest, s, best);
                            s.cost_total -= curr.connectsTo[i].cost;
                            s.path.RemoveAt(s.path.Count - 1);
                        }
                    }
                }
            }
            return best;
        }

        public static SolutionPathA backTracking_fiable(Node[] nodes, Node curr, Server dest, SolutionPathA s, SolutionPathA best)
        {
            int i;

            // Mirem si és la primera vegada que entri a la funció
            if (best.reliability_total == 1)
            {
                // En aquest cas, incialitzem el reliability_total al primer valor de fiabilitat
                s.reliability_total = curr.reliability;

                // Afegim aquest primer node al camí ja que hem passat per aquí
                s.path.Add(curr);

                // Per no donar problemes amb la comparació, fiquem el valor de la millor fiabilitat a 0
                best.reliability_total = 0;
            }

            // cas trivial
            if (dest_reached(curr, dest))
            {
                if (s.reliability_total > best.reliability_total)
                {
                    copySolution(s, ref best);
                }
            }
            else // cas no trivial
            {
                for (i = 0; i < curr.connectsTo.Length; i++)
                {
                    // Si trobem el node següent a l'array de connexions, mirem el següent
                    if (!nodeInPath(nodes[curr.connectsTo[i].to - 1], s))
                    {
                        // Mirem si la fiabilitat amb el node següent és la millor
                        if (s.reliability_total * nodes[curr.connectsTo[i].to - 1].reliability > best.reliability_total)
                        {
                            // Afegim el "node següent" actual al camí de la solució i multipliquem la fiabiltitat al nou node
                            s.path.Add(nodes[curr.connectsTo[i].to - 1]);
                            s.reliability_total *= nodes[curr.connectsTo[i].to - 1].reliability;

                            // Crida recusriva de backtracking
                            best = backTracking_fiable(nodes, nodes[curr.connectsTo[i].to - 1], dest, s, best);

                            // Anirem cap a dalt per buscar altres camins
                            s.reliability_total /= nodes[curr.connectsTo[i].to - 1].reliability;
                            s.path.RemoveAt(s.path.Count - 1);
                        }
                    }
                }
            }
            return best;
        }
        public static SolutionD backtrackingD(Usuari[] users, Server[] servers)
        {
            // Inicialització de la solució
            SolutionD solutionfinal = initSolutionDistribution(servers);
            SolutionD solutionactual = new SolutionD();
            copySolution(solutionfinal, ref solutionactual);
            solutionactual.equitativitat = calculaEquitativitat(solutionactual);

            solutionfinal = backtracking_distribution(solutionactual, ref solutionfinal, 0, users, servers);
            printSolutionD(solutionfinal);
            Console.WriteLine("\n\tNombre de crides recursives: " + numCridesRec);

            // Reinicialitzar per poder reutilitzar en executar altres algorismes
            numCridesRec = 0;
            return solutionfinal;
        }

        public static SolutionD backtracking_distribution(SolutionD sol, ref SolutionD best, int n, Usuari[] users, Server[] servers)
        {
            numCridesRec++;

            sol.equitativitat = calculaEquitativitat(sol);
            sol.localitzacio = calculaLocalitzacio(ref sol, users);
            if (isSolutionD(sol, users.Length))
            {
                if (isPromisingD(sol, best))
                {
                    copySolution(sol, ref best);
                }
            }
            else
            {
                int numUsers = users.Length;
                int numServers = servers.Length;
                if (isPromisingD(sol, best))
                {
                    for (int m = 0; m < numServers; m++)
                    {
                        // Afegim l'usuari a la llista d'usuaris del servidor
                        Distribution dist = (Distribution)sol.distribucio[m];
                        DistributedUser user = initDistributedUser(n, users);
                        dist.users.Add(user);
                        dist.activitat_total += users[n].activity;
                        sol.distribucio[m] = dist;
                        sol.numUsersDistribuits++;
                        ++n;

                        // Crida recursiva de backtracking
                        best = backtracking_distribution(sol, ref best, n, users, servers);

                        // Tirem enrere treient l'usuari de la llista del servidor
                        --n;
                        dist = (Distribution)sol.distribucio[m];
                        dist.users.RemoveAt(dist.users.Count - 1);
                        dist.activitat_total -= users[n].activity;
                        sol.numUsersDistribuits--;
                        sol.distribucio[m] = dist;
                    }
                }
            }
            return best;
        }
        
        public static bool isSolutionD(SolutionD sol, SolutionD best, int numUsersTotal)
        {
            return (sol.numUsersDistribuits == numUsersTotal && isPromisingD(sol, best));
        }
        public static bool isSolutionD(SolutionD sol, int numUsersTotal)
        {
            return (sol.numUsersDistribuits == numUsersTotal);
        }

        public static bool isPromisingD(SolutionD sol, SolutionD best)
        {
            // Mirem si la solució és millor que la best
            if (sol.equitativitat < best.equitativitat + tolerancia)
                return true;
            else if (best.equitativitat - tolerancia <= sol.equitativitat && sol.equitativitat <= best.equitativitat + tolerancia)
            {
                // En cas de que sigui promising, passarem a mirar si estan ben localitzats els usuaris
                if (sol.localitzacio > best.localitzacio)
                    return true;
                else
                    return false;
            }
            else if (sol.equitativitat > best.equitativitat - tolerancia) // Si és major, mai serà millor que la best
                return false;
            else
                return false;
        }

        public static DistributedUser initDistributedUser(int n, Usuari[] users)
        {
            DistributedUser user_dist = new DistributedUser();
            user_dist.user = users[n];
            user_dist.localitzat = false;
            return user_dist;
        }

        public static void copySolution(SolutionD org, ref SolutionD dest)
        {
            dest.equitativitat = org.equitativitat;
            dest.localitzacio = org.localitzacio;
            dest.numUsersDistribuits = org.numUsersDistribuits;
            dest.distribucio = copyDistribution(org.distribucio);
            dest.currServerIndex = org.currServerIndex;
            dest.currUserIndex = org.currUserIndex;
        }

        public static ArrayList copyDistribution(ArrayList orgArrServers)
        {
            int i = 0;
            ArrayList destination = new ArrayList();
            // For each server, copy the information
            for (i = 0; i < orgArrServers.Count; i++)
            {
                Distribution serverOrg = (Distribution)orgArrServers[i];
                Distribution serverDest = new Distribution();
                serverDest.activitat_total = serverOrg.activitat_total;
                serverDest.s = serverOrg.s;
                // Copy the contents of the ArrayList
                serverDest.users = copyContents(serverOrg.users);
                destination.Add(serverDest);
            }
            return destination;
        }

        public static void printSolutionD(SolutionD sol)
        {

            Console.Write("\n");
            for (int l = 0; l < sol.distribucio.Count; l++)
            {
                Console.Write("\t");
                Distribution dist = (Distribution)sol.distribucio[l];
                Console.Write("Server " + dist.s.id + ": ");
                for (int k = 0; k < dist.users.Count; k++)
                {
                    DistributedUser u = (DistributedUser)dist.users[k];

                    Console.Write(u.user.username + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("\n\tEquitativitat: " + sol.equitativitat);
            Console.WriteLine("\tUsuaris localitzats correctament: " + sol.localitzacio);
        }

        public static SolutionD initSolutionDistribution(Server[] servers)
        {
            SolutionD sol = new SolutionD();
            int i = 0;

            sol.distribucio = new ArrayList(); // de tipus Distribucio
            sol.equitativitat = double.MaxValue;
            sol.localitzacio = 0;
            sol.numUsersDistribuits = 0;
            sol.currServerIndex = 0;
            sol.currUserIndex = 0;

            // Col·locarem els diferents servidors en l'ArrayList
            for (i = 0; i < servers.Length; i++)
            {
                Distribution dist = new Distribution();
                dist.s = servers[i];
                dist.users = new ArrayList(); // de tipus Distributed Users
                dist.activitat_total = 0;

                sol.distribucio.Add(dist);
            }
            return sol;
        }

        // Compta quants usuaris estan localitzats en total
        public static int countUsersLocalitzats(SolutionD sol)
        {
            int i = 0, j = 0;
            int numUsers = 0;
            int total = 0;
            for (i = 0; i < sol.distribucio.Count; i++)
            {
                Distribution serv = (Distribution)sol.distribucio[i];
                numUsers = serv.users.Count;
                for (j = 0; j < numUsers; j++)
                {
                    DistributedUser user = (DistributedUser)serv.users[j];
                    if (user.localitzat)
                    {
                        total++;
                    }
                }
            }
            return total;
        }


        public class myEquitativitatComparer : IComparer
        {

            int IComparer.Compare(Object x, Object y)
            {
                Distribution actual = (Distribution)x;
                Distribution nou = (Distribution)y;

                if (actual.activitat_total < nou.activitat_total)
                    return -1;
                else if (actual.activitat_total == nou.activitat_total)
                    return 0;
                else
                    return 1;
            }

        }

        // Compara dues solucions de tipus SolutionD per al problema de la distribució
        public class mySolutionDComparer : IComparer
        {

            int IComparer.Compare(Object x, Object y)
            {
                SolutionD solActual = (SolutionD)x;
                SolutionD solFinal = (SolutionD)y;

                if (solActual.equitativitat < solFinal.equitativitat)
                    return -1;
                else if (solActual.equitativitat == solFinal.equitativitat)
                    return 0;
                else
                    return 1;
            }

        }

        // Calcula l'equitativitat de la solució fent la resta de l'activitat total del servidor amb major activitat
        // i l'activitat total del servidor amb menor activitat
        public static double calculaEquitativitat(SolutionD sol)
        {
            IComparer myComparer = new myEquitativitatComparer();

            // Necessitem una solució auxiliar per no canviar l'ordre de l'arraylist original
            SolutionD temp = new SolutionD();
            copySolution(sol, ref temp);

            // Fem un sort segons l'activitat
            temp.distribucio.Sort(myComparer);
            Distribution mes_activitat = (Distribution)temp.distribucio[temp.distribucio.Count - 1];
            Distribution menys_activitat = (Distribution)temp.distribucio[0];

            // Fem la resta
            return mes_activitat.activitat_total - menys_activitat.activitat_total;
        }

        // Compta quants usuaris estan ben localitzats segons ubicació
        public static int calculaLocalitzacio(ref SolutionD sol, Usuari[] origUsers)
        {
            int numUsers = 0;
            for (int i = 0; i < sol.distribucio.Count; i++)
            {
                // Get each server
                Distribution serv = (Distribution)sol.distribucio[i];
                for (int j = 0; j < serv.users.Count; j++)
                {
                    DistributedUser userDist = (DistributedUser)serv.users[j];
                    Usuari userOrg = findUser(userDist.user.username, origUsers);

                    // Mirem si el servidor on està l'usuari distribuït és igual al id del usuari en l'array original
                    if (serv.s.id == userOrg.server)
                    {
                        userDist.localitzat = true;
                        numUsers++;
                    }
                    else
                    {
                        userDist.localitzat = false;
                    }
                    serv.users[j] = userDist;
                }
                sol.distribucio[i] = serv;
            }
            return numUsers;
        }

        public static Usuari findUser(string nom, Usuari[] users)
        {
            for (int i = 0; i < users.Length; i++)
            {
                if (nom == users[i].username)
                    return users[i];
            }
            return null;
        }

        public static SolutionPathA[] branchAndBoundA(Node[] nodes, Server server_orig, Server server_dest)
        {
            SolutionPathA[] solution = new SolutionPathA[2];
            int i = 0;
            int num_origin_nodes = server_orig.reachable_from.Length;
            // Per al camí amb menor cost
            SolutionPathA solActual = new SolutionPathA();
            SolutionPathA solFinal = new SolutionPathA();
            SolutionPathA solExtra = new SolutionPathA(); // On guardem el best diguem

            // Per al camí amb millor fiabilitat
            SolutionPathA solActual_fiable = new SolutionPathA();
            SolutionPathA solFinal_fiable = new SolutionPathA();
            SolutionPathA solExtra_fiable = new SolutionPathA(); // On guardem el best diguem

            // S'hauria de comprovar diferents camins per cada node a on està connectat el servidor
            // ja que el servidor pot està accesible des de diferents nodes
            // Anirem comparant el camí per cada node de connexió al servidor i mirant quin és el millor 

            // COST
            // Inicialitzar les solucions
            solFinal.cost_total = int.MaxValue;
            solFinal.path = new ArrayList();
            solActual.path = new ArrayList();

            // suposant que els nodes del JSON estan ordenats per id
            solActual = branchAndBound_cost(nodes, nodes[server_orig.reachable_from[i++] - 1], server_dest, solFinal);
            copySolution(solActual, ref solFinal);
            copySolution(solActual, ref solExtra);

            while (i < num_origin_nodes)
            {
                // Calculem l'altra solució
                solActual = branchAndBound_cost(nodes, nodes[server_orig.reachable_from[i] - 1], server_dest, solFinal);

                // Comprovem si aquesta nova solució és millor que l'anterior
                if (solActual.cost_total < solExtra.cost_total)
                {
                    copySolution(solActual, ref solExtra);
                }
                i++;
            }


            // RELIABILITY
            // Inicialitzar les solucions i comptador
            i = 0;
            solFinal_fiable.reliability_total = 1;
            solFinal_fiable.path = new ArrayList();
            solActual_fiable.path = new ArrayList();

            // suposant que els nodes del JSON estan ordenats per id
            solActual_fiable = branchAndBound_fiable(nodes, nodes[server_orig.reachable_from[i++] - 1], server_dest, solFinal_fiable);
            copySolution(solActual_fiable, ref solFinal_fiable);
            copySolution(solActual_fiable, ref solExtra_fiable);

            while (i < num_origin_nodes)
            {
                // Calculem l'altra solució
                solActual_fiable = branchAndBound_fiable(nodes, nodes[server_orig.reachable_from[i++] - 1], server_dest, solFinal_fiable);

                // Comprovem si aquesta nova solució és millor que l'anterior
                if (solActual_fiable.reliability_total > solExtra_fiable.reliability_total)
                {
                    copySolution(solActual_fiable, ref solExtra_fiable);
                }
                i++;
            }

            solution[0] = solExtra;
            solution[1] = solExtra_fiable;

            return solution;
        }

        public static SolutionPathA branchAndBound_fiable(Node[] nodes, Node curr, Server dest, SolutionPathA best)
        {
            ReliabilityComparer comp = new ReliabilityComparer();
            PriorityQueue path_queue = new PriorityQueue(comp); // live_nodes
            SolutionPathA sol = new SolutionPathA(); // x
            ArrayList options = new ArrayList();

            // Inicialització de la primera solució des del primer node (origen)
            best.reliability_total = 1;
            sol.reliability_total = curr.reliability;
            sol.path = new ArrayList();
            sol.path.Add(curr);

            path_queue.Enqueue(sol);

            while (path_queue.Count != 0)
            {
                sol = (SolutionPathA)path_queue.Dequeue();

                // Si a la primera, ja hem arribat al final
                if (isSolutionA_fiable(sol, dest, best))
                {
                    copySolution(sol, ref best);
                }
                else
                {
                    // Busquem totes les solucions a partir del node
                    options = expand(nodes, sol);

                    // Reinicialitzar abans de comparar
                    if (best.reliability_total == 1)
                    {
                        best.reliability_total = 0;
                    }

                    // Mirem per cada opció si és "promising" o si ja hem arribat al final
                    foreach (SolutionPathA opt in options)
                    {
                        // cas trivial: mirem si ja hem arribat al destí
                        if (isSolutionA_fiable(opt, dest, best))
                        {
                            // Referim aquest mínim com a millor solució
                            copySolution(opt, ref best);
                        }
                        else
                        {
                            // mirem si té un cost menor que el best perquè no ens servirà si té més cost
                            if (isPromisingA_fiable(opt, best))
                            {
                                path_queue.Enqueue(opt);
                            }
                        }
                    }
                }

            }

            return best;
        }

        // Expandim a partir del node i sumar la distancia a cada node
        public static ArrayList expand(Node[] nodes, SolutionPathA sol)
        {
            ArrayList options = new ArrayList();
            Node last_node = (Node)sol.path[sol.path.Count - 1];
            for (int i = 0; i < last_node.connectsTo.Length; i++)
            {
                if (!nodeInPath(nodes[last_node.connectsTo[i].to - 1], sol))
                {
                    SolutionPathA solution_nivel = new SolutionPathA();
                    copySolution(sol, ref solution_nivel);
                    solution_nivel.path.Add(nodes[last_node.connectsTo[i].to - 1]);
                    solution_nivel.cost_total += last_node.connectsTo[i].cost;
                    solution_nivel.reliability_total *= nodes[last_node.connectsTo[i].to - 1].reliability;
                    options.Add(solution_nivel);
                }
            }
            return options;
        }

        public static SolutionPathA getMinCostSolution(SolutionPathA a, SolutionPathA b)
        {
            if (a.cost_total < b.cost_total)
                return a;
            else
                return b;
        }

        public static bool isSolutionA(SolutionPathA opt, Server dest, SolutionPathA best)
        {
            Node lastNode = (Node)opt.path[opt.path.Count - 1];
            return (dest_reached(lastNode, dest) && isPromisingA(opt, best));
        }

        public static bool isPromisingA(SolutionPathA opt, SolutionPathA best)
        {
            return opt.cost_total < best.cost_total;
        }

        public static bool isSolutionA_fiable(SolutionPathA opt, Server dest, SolutionPathA best)
        {
            Node lastNode = (Node)opt.path[opt.path.Count - 1];
            return (dest_reached(lastNode, dest) && isPromisingA_fiable(opt, best));
        }

        public static bool isPromisingA_fiable(SolutionPathA opt, SolutionPathA best)
        {
            return opt.reliability_total > best.reliability_total;
        }

        public static SolutionPathA branchAndBound_cost(Node[] nodes, Node curr, Server dest, SolutionPathA best)
        {

            PriorityQueue path_queue = new PriorityQueue(); // live_nodes
            SolutionPathA sol = new SolutionPathA(); // x
            ArrayList options = new ArrayList();

            // Inicialització de la primera solució des del primer node (origen)
            sol.path = new ArrayList();
            sol.path.Add(curr);

            path_queue.Enqueue(sol);

            while (path_queue.Count != 0)
            {
                sol = (SolutionPathA)path_queue.Dequeue();

                // Si a la primera, ja hem arribat al final
                if (isSolutionA(sol, dest, best))
                {
                    copySolution(sol, ref best);
                }
                else
                {
                    // Busquem totes les solucions a partir del node
                    options = expand(nodes, sol);

                    // Mirem per cada opció si és "promising" o si ja hem arribat al final
                    foreach (SolutionPathA opt in options)
                    {
                        // cas trivial: mirem si ja hem arribat al destí
                        if (isSolutionA(opt, dest, best))
                        {
                            // Referim aquest mínim com a millor solució
                            copySolution(opt, ref best);
                        }
                        else
                        {
                            // mirem si té un cost menor que el best perquè no ens servirà si té més cost
                            if (isPromisingA(opt, best))
                            {
                                path_queue.Enqueue(opt);
                            }
                        }
                    }
                }

            }

            return best;
        }

        private class ReliabilityComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                SolutionPathA sol_x = (SolutionPathA)x;
                SolutionPathA sol_y = (SolutionPathA)y;

                if (sol_x.reliability_total < sol_y.reliability_total)
                {
                    return 1;
                }
                else if (sol_x.reliability_total > sol_y.reliability_total)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static SolutionD branchAndBoundD(Usuari[] users, Server[] servers)
        {
            // Inicialització de la solució
            SolutionD solutionfinal = initSolutionDistribution(servers);
            SolutionD solutionactual = new SolutionD();
            copySolution(solutionfinal, ref solutionactual);
            solutionactual.equitativitat = calculaEquitativitat(solutionactual);

            solutionfinal = bb_distribution(solutionfinal, 0, 0, users, servers);
            printSolutionD(solutionfinal);
            Console.WriteLine("\tNombre de iteracions: " + numCridesRec);

            // Reinicialitzar per poder reutilitzar en executar altres algorismes
            numCridesRec = 0;
            return solutionfinal;
        }

        public static SolutionD bb_distribution(SolutionD best, int m, int n, Usuari[] users, Server[] servers)
        {
            IComparer mySolComparer = new mySolutionDComparer();
            PriorityQueue distribution_queue = new PriorityQueue(mySolComparer); // live_nodes
            SolutionD sol = initSolutionDistribution(servers); // x
            ArrayList options = new ArrayList();
            sol.currServerIndex = m;

            // Inicialització de la primera solució afegint el primer usuari al servidor 1
            // Afegim l'usuari a la llista d'usuaris del servidor
            Distribution dist = (Distribution)sol.distribucio[sol.currServerIndex];
            DistributedUser user = initDistributedUser(n, users);
            dist.users.Add(user);
            dist.activitat_total += user.user.activity;
            sol.distribucio[sol.currServerIndex] = dist;
            sol.numUsersDistribuits++;
            sol.currUserIndex = n;

            distribution_queue.Enqueue(sol);

            while (distribution_queue.Count != 0)
            {
                // Comptem una iteració més
                numCridesRec++;

                // Agafem la millor solució de la cua
                sol = (SolutionD)distribution_queue.Dequeue();

                sol.equitativitat = calculaEquitativitat(sol);
                sol.localitzacio = calculaLocalitzacio(ref sol, users);

                // Si a la primera, ja hem arribat al final
                if (isSolutionD(sol, users.Length))
                {
                    copySolution(sol, ref best);
                }
                else
                {
                    // Busquem totes les solucions a partir de l'usuari on estem ara
                    // Expandir les solucions posant el següent usuari però en diferents servidors 
                    // Vol dir que estem fent l'expand per fer l'amplada de l'espai 
                    options = expand(servers, users, sol);

                    // Mirem per cada opció si és "promising" o si ja hem arribat al final
                    foreach (SolutionD opt in options)
                    {
                        // Cas trivial: mirem si ja hem arribat al destí
                        if (isSolutionD(opt, users.Length))
                        {
                            // Referim aquest mínim com a millor solució
                            copySolution(opt, ref best);
                        }
                        else
                        {
                            if (isPromisingD(opt, best))
                            {
                                distribution_queue.Enqueue(opt);
                            }
                        }
                    }
                }

            }
            return best;
        }

        public static ArrayList expand(Server[] servers, Usuari[] users, SolutionD sol)
        {
            ArrayList options = new ArrayList();

            // Get the last server where we put the last user
            Distribution currServ = (Distribution)sol.distribucio[sol.currServerIndex];

            for (int i = 0; i < servers.Length; i++)
            {
                // Crear una nova solució a partir de les dades de la solució actual
                SolutionD solution_nivel = new SolutionD();
                copySolution(sol, ref solution_nivel);

                solution_nivel.currUserIndex++;

                // Afegim el usuari al servidor que toca. Sempre partirem de l'últim usuari distribuït.
                Distribution dist = (Distribution)solution_nivel.distribucio[i];
                DistributedUser u = initDistributedUser(solution_nivel.currUserIndex, users);
                dist.users.Add(u);
                dist.activitat_total += u.user.activity;
                solution_nivel.distribucio[i] = dist;
                solution_nivel.numUsersDistribuits++;
                solution_nivel.currServerIndex = i;

                // Calculem l'equitativitat de la nova solució
                solution_nivel.equitativitat = calculaEquitativitat(solution_nivel);
                solution_nivel.localitzacio = calculaLocalitzacio(ref solution_nivel, users);

                // Afegim la nova solució a la llista d'opcions 
                options.Add(solution_nivel);
            }

            return options;
        }

        public static SolutionPathA[] greedyA(Node[] n, Server servA, Server servB)
        {
            int node_id; bool reached = false;

            SolutionPathA[] solution = new SolutionPathA[2];
            int servA_children = servA.reachable_from.Length;

            //Solució per el millor camí amb menor cost
            SolutionPathA solActual = new SolutionPathA();
            SolutionPathA solExtra = new SolutionPathA();
            SolutionPathA solReturnCost = new SolutionPathA();

            //Inicialitzem la solució
            solActual.path = new ArrayList();
            solActual.cost_total = int.MaxValue;
            solExtra.path = new ArrayList();
            solReturnCost.path = new ArrayList();

            //Mirem si en algun fill node ja es pot arribar al servidor destí
            for (int i = 0; i < servA.reachable_from.Length && !reached; i++)
            {
                node_id = servA.reachable_from[i] - 1;
                reached = dest_reached(n[node_id], servB);
                if (reached)
                {
                    solReturnCost.path.Add(n[node_id]);
                    solReturnCost.cost_total = 0;
                }
            }

            if (!reached)
            {
                solActual = selectChild(n, servA, servB);
                copySolution(solActual, ref solExtra);
                
                solReturnCost = greedyCost(n, servB, solExtra);
            }

            //Solució per el millor camí amb major fiabilitat
            SolutionPathA solActual_fiable = new SolutionPathA();
            SolutionPathA solExtra_fiable = new SolutionPathA();
            SolutionPathA solReturnFiable = new SolutionPathA();

            solActual_fiable.path = new ArrayList();
            solActual.reliability_total = 1;
            solExtra_fiable.path = new ArrayList();

            solActual_fiable = selectChildFiable(n, servA, servB, solActual_fiable, 0);
            solReturnFiable = greedFiable(n, servB, solActual_fiable);

            solution[0] = solReturnCost;
            solution[1] = solReturnFiable;

            return solution;
        }
        public static void findingBestWayGreedyA(int total, ref SolutionPathA solActual, ref SolutionPathA solExtra, Node[] n, Server servA, Server servB)
        {
            int i = 1;
            while (i < total)
            {
                cleanSolution(ref solActual, 0);
                solActual = selectChild(n, servA, servB);
                if (solActual.cost_total < solExtra.cost_total)
                {
                    copySolution(solActual, ref solExtra);
                }
                i++;
            }
        }

        public static SolutionPathA greedFiable(Node[] n, Server serv_dest, SolutionPathA solInicial)
        {
            SolutionPathA solFinal = new SolutionPathA();
            copySolution(solInicial, ref solFinal);

            bool reached = false; bool newId = false;
            int i = 1;
            int id = 0;

            while (!reached)
            {
                Node[] arrayNode = solFinal.path.ToArray(typeof(Node)) as Node[];

                int last_node_id = arrayNode[arrayNode.Length - 1].id - 1;
                if (dest_reached(n[last_node_id], serv_dest)) { reached = true; }
                else
                {

                    //id del nodo ultimo
                    int node_id = arrayNode[arrayNode.Length - 1].id - 1;
                    double init_fiable = 0;

                    for (int k = 0; k < n[node_id].connectsTo.Length; k++)
                    {
                        int idaux = n[node_id].connectsTo[k].to;

                        // controlem si el node ja forma part del camí
                        if (!nodeInPath(n[idaux - 1], solFinal))
                        {
                            // agafem el primer com a referència
                            if (k == 0)
                            {
                                init_fiable = n[idaux - 1].reliability;
                                id = n[node_id].connectsTo[k].to;
                                newId = true;
                            }
                            if (n[idaux - 1].reliability > init_fiable)
                            {
                                init_fiable = n[node_id].reliability;
                                id = n[node_id].connectsTo[k].to;
                                newId = true;
                            }
                        }

                    }
                    if (newId && !nodeInPath(n[id - 1], solFinal))
                    {
                        solFinal.path.Add(n[id - 1]);
                        solFinal.reliability_total *= init_fiable;
                    }
                }
            }

            return solFinal;
        }

        public static SolutionPathA greedyCost(Node[] n, Server serv_dest, SolutionPathA solInicial)
        {
            SolutionPathA solFinal = new SolutionPathA();
            copySolution(solInicial, ref solFinal);

            bool reached = false;
            bool ok = false;
            int i = 1;
            int id = 0;
            int init_cost = 0;
            while (!reached)
            {
                Node[] arrayNode = solFinal.path.ToArray(typeof(Node)) as Node[];

                int last_node_id = arrayNode[arrayNode.Length - 1].id - 1;

                // cas trivial, mirem si ja hem arribat al destí
                if (dest_reached(n[last_node_id], serv_dest))
                {
                    reached = true;
                }
                else
                {
                    //id de l'ultim node
                    int node_id = arrayNode[arrayNode.Length - 1].id - 1;
                    init_cost = int.MaxValue;
                    id = n[node_id].connectsTo[0].to;
                    i = 0;
                    ok = false;
                    while (i < n[node_id].connectsTo.Length)
                    {
                        // mirem si el node que estem analitzant ja està en el path
                        if (!nodeInPath(n[n[node_id].connectsTo[i].to - 1], solFinal))
                        {
                            ok = true;
                            //escogemos cual es el de menor distancia
                            if (n[node_id].connectsTo[i].cost < init_cost)
                            {
                                init_cost = n[node_id].connectsTo[i].cost;
                                id = n[node_id].connectsTo[i].to;
                            }
                        }
                        i++;

                    }
                    if (id != 0)
                    {
                        // si el node no està en el path, l'afegim
                        if (!nodeInPath(n[id - 1], solFinal))
                        {
                            solFinal.path.Add(n[id - 1]);
                            solFinal.cost_total += init_cost;
                        }
                    }
                    else
                    {
                        // si és id és 0, vol dir que és el primer node
                        // per tant, l'afegim
                        solFinal.path.Add(n[id]);
                        solFinal.cost_total += init_cost;
                        reached = true;
                    }
                    if (!ok)
                    {
                        return solFinal;
                    }
                }

            }

            return solFinal;
        }

        public static SolutionPathA chosenChild(List<SolutionPathA> c)
        {
            SolutionPathA s = new SolutionPathA();
            int index = 0;
            int best = int.MaxValue;
            for (int i = 0; i < c.Count; i++)
            {
                if (c[i].cost_total < best)
                {
                    best = c[i].cost_total;
                    index = i;
                }
            }
            s.cost_total = best;
            s.path = c[index].path;

            return s;
        }

        public static SolutionPathA selectChild(Node[] n, Server origin, Server dest)
        {
            List<SolutionPathA> childs = new List<SolutionPathA>();
            SolutionPathA s = new SolutionPathA();
            s.path = new ArrayList();
            int node_id = origin.reachable_from[0] - 1;

            if (origin.reachable_from.Length > 1)
            {
                for (int j = 0; j < origin.reachable_from.Length; j++)
                {
                    s.path = new ArrayList();
                    ChoosenChild cd = new ChoosenChild();
                    cd.cost = int.MaxValue;
                    int id = origin.reachable_from[j] - 1;
                    s.path.Add(n[id]);

                    // Mirem tots els nodes que estan connectats al node actual
                    for (int i = 0; i < n[id].connectsTo.Length; i++)
                    {
                        if (n[id].connectsTo[i].cost < cd.cost)
                        {
                            cd.cost = n[id].connectsTo[i].cost;
                            cd.id = n[id].connectsTo[i].to;
                        }

                    }
                    s.path.Add(n[cd.id - 1]);
                    s.cost_total = cd.cost;
                    childs.Add(s);
                }
                s = chosenChild(childs);

            }
            else
            {
                s.path.Add(n[node_id]);
                s.cost_total = 0;
            }
            return s;



        }

        public static SolutionPathA selectChildFiable(Node[] n, Server origin, Server dest, SolutionPathA s, int index)
        {
            int id = origin.reachable_from[0] - 1;
            int id_aux = 0;
            int node_id = origin.reachable_from[0] - 1;
            double init_fiable = double.MinValue;

            if (origin.reachable_from.Length > 1)
            {
                for (int i = 0; i < origin.reachable_from.Length; i++)
                {
                    id = origin.reachable_from[i] - 1;
                    if (n[id].reliability > init_fiable)
                    {
                        init_fiable = n[id].reliability;
                        id_aux = n[id].id - 1;
                    }
                }
                s.path.Add(n[id_aux]);
                s.reliability_total = init_fiable;
            }
            else
            {
                s.path.Add(n[id]);
                s.reliability_total = n[id].reliability;
            }
            return s;
        }

        public static void greedyD (Usuari[] users, Server[] servers)
        {
            SolutionD solFinal = initSolutionDistribution(servers);
            solFinal = greedy_distribution(users, servers, solFinal, 0,0);
            printSolutionD(solFinal);
        }

        public static SolutionD greedy_distribution (Usuari[] users, Server[] s, SolutionD best, int m, int n)
        {
            ArrayList options = new ArrayList();
            SolutionD sol = initSolutionDistribution(s);
            sol.currServerIndex = m;

            Distribution dist = (Distribution)sol.distribucio[sol.currServerIndex];
            DistributedUser user = initDistributedUser(n, users);
            dist.users.Add(user);
            dist.activitat_total += user.user.activity;
            sol.distribucio[sol.currServerIndex] = dist;
            sol.numUsersDistribuits++;
            sol.currUserIndex = n;

            //Inicialitzem la solució abans de fer el Greedy
            sol.equitativitat = calculaEquitativitat(sol);
            sol.localitzacio = calculaLocalitzacio(ref sol, users);

            copySolution(sol, ref best);

            do
            {
                options = expand(s, users, best);
                best = (SolutionD)options[0];
                foreach (SolutionD opt in options)
                {
                    if (isPromisingD(opt, best))
                    {
                        best = opt;
                    }
                }
            } while (best.currUserIndex < users.Length - 1);
            
            return best;

        }
        public static SolutionPathA[] greedyBacktrackingA (Node[] n, Server servA, Server servB)
        {
            SolutionPathA[] solution = new SolutionPathA[2];
            
            ArrayList nodes = expandChildServer(servA, n);

            solution[0].path = new ArrayList();
            SolutionPathA solGreedy = new SolutionPathA();
            SolutionPathA solBackTracking = new SolutionPathA();
            SolutionPathA solExtra = new SolutionPathA(); //On guardem el best

            solGreedy.cost_total = 0;
            solGreedy.path = new ArrayList();
            solBackTracking.cost_total = 0;
            solBackTracking.path = new ArrayList();

            SolutionPathA solGreedyFiable = new SolutionPathA();
            SolutionPathA solBTFiable = new SolutionPathA();
            SolutionPathA solExtraFiable = new SolutionPathA();

            solGreedyFiable.reliability_total = 1;
            solGreedyFiable.path = new ArrayList();
            solBTFiable.reliability_total = 1;
            solBTFiable.path = new ArrayList();

            //Per cada node del servidor, farem un greedy
            for (int i = 0; i < nodes.Count; i++)
            {
                solGreedy.path.Add(nodes[i]);
                copySolution(solGreedy, ref solExtra);
                solGreedy = greedyCost(n, servB, solExtra);

                if (solGreedy.cost_total != 0)
                {
                    //Farem un BackTracking per cada fill del server per veure quin es el millor camí
                    solBackTracking = backTracking_cost(n, n[servA.reachable_from[i] - 1], servB, solution[0], solGreedy);
                    copySolution(solBackTracking, ref solExtra);
                }
                cleanSolution(ref solGreedy, 0);
                cleanSolution(ref solBackTracking, 0);
            }

            for (int i = 0; i < nodes.Count; i++)
            {
                // Calculem fiabilitat
                solGreedyFiable.path.Add(nodes[i]);
                copySolution(solGreedyFiable, ref solExtraFiable);
                solGreedyFiable = greedFiable(n, servB, solExtraFiable);

                if (solGreedyFiable.reliability_total != 1)
                {
                    solBTFiable = backTracking_fiable(n, n[servA.reachable_from[i] - 1], servB, solution[0], solExtraFiable);
                    copySolution(solBTFiable, ref solExtraFiable);

                }
                else
                {
                    Node aux = (Node)solGreedyFiable.path[0];
                    solExtraFiable.reliability_total = aux.reliability;
                }
                cleanSolution(ref solGreedyFiable, 1);
                cleanSolution(ref solBTFiable, 1);

            }

            solution[0] = solExtra;
            solution[1] = solExtraFiable;
            return solution;

        }
        public static void greedyBacktrackingD (Server[] servers, Usuari[] users)
        {
            SolutionD solGreedyFinal = initSolutionDistribution(servers);
            SolutionD solutionactual = initSolutionDistribution(servers);
            SolutionD solBTFinal = new SolutionD();

            // Fem greedy per la primera solució de referencia
            solGreedyFinal = greedy_distribution(users, servers, solGreedyFinal, 0, 0);
            
            // Inicialitzem la primera solució per al backtracking
            solutionactual.equitativitat = calculaEquitativitat(solutionactual);

            // Calculem la solucií final, agafant la solució de greedy com a best
            solBTFinal = backtracking_distribution(solutionactual, ref solGreedyFinal, 0, users, servers);

            printSolutionD(solBTFinal);
            Console.WriteLine("\n\tNombre de crides recursives: " + numCridesRec);

            // Reinicialitzar per poder reutilitzar en executar altres algorismes
            numCridesRec = 0;
        }

        public static ArrayList expandChildServer(Server origin, Node[] n)
        {
            ArrayList list = new ArrayList();
            for (int i = 0; i < origin.reachable_from.Length; i++)
            {
                int nodeId = origin.reachable_from[i] - 1;
                list.Add(n[nodeId]);

            }
            return list;
        }

        public static SolutionPathA[] greedyBranchAndBoundA(Node[] n, Server servA, Server servB)
        {
            SolutionPathA[] solution = new SolutionPathA[2];
            ArrayList nodes = expandChildServer(servA, n);

            // Per al camí amb menor cost
            SolutionPathA solGreedy = new SolutionPathA();
            SolutionPathA solBB = new SolutionPathA();
            SolutionPathA solGreedyFiable = new SolutionPathA();
            SolutionPathA sollBBFiable = new SolutionPathA();
            SolutionPathA solExtra = new SolutionPathA(); // On guardem el best diguem
            SolutionPathA solExtraFiable = new SolutionPathA();

            // Inicialitzar les solucions
            solGreedy.cost_total = 0;
            solGreedy.path = new ArrayList();
            solBB.cost_total = 0;
            solBB.path = new ArrayList();

            solGreedyFiable.reliability_total = 1;
            solGreedyFiable.path = new ArrayList();
            sollBBFiable.reliability_total = 1;
            sollBBFiable.path = new ArrayList();

            //Per cada node del servidor, farem un greedy
            for (int i = 0; i < nodes.Count; i++)
            {
                solGreedy.path.Add(nodes[i]);
                copySolution(solGreedy, ref solExtra);
                solGreedy = greedyCost(n, servB, solExtra);

                if (solGreedy.cost_total != 0)
                {
                    //Farem un Branch and Bound per cada fill del server per veure quin es el millor camí
                    solBB = branchAndBound_cost(n, n[servA.reachable_from[i] - 1], servB, solGreedy);
                    copySolution(solBB, ref solExtra);
                }
                cleanSolution(ref solGreedy, 0);
                cleanSolution(ref solBB, 0);
            }
            for (int i = 0; i < nodes.Count; i++)
            {
                // Calculem fiabilitat
                solGreedyFiable.path.Add(nodes[i]);
                copySolution(solGreedyFiable, ref solExtraFiable);
                solGreedyFiable = greedFiable(n, servB, solExtraFiable);

                if (solGreedyFiable.reliability_total != 1)
                {
                    sollBBFiable = branchAndBound_fiable(n, n[servA.reachable_from[i] - 1], servB, solExtraFiable);
                    copySolution(sollBBFiable, ref solExtraFiable);

                }
                else
                {
                    Node aux = (Node)solGreedyFiable.path[0];
                    solExtraFiable.reliability_total = aux.reliability;
                }
                cleanSolution(ref solGreedyFiable, 1);
                cleanSolution(ref sollBBFiable, 1);
            }
            solution[0] = solExtra;
            solution[1] = solExtraFiable;
            return solution;

        }
        public static void greedyBranchAndBoundD(Server[] servers, Usuari[] users)
        {
            SolutionD solGreedyFinal = initSolutionDistribution(servers);
            SolutionD solBBFinal = new SolutionD();
            
            solGreedyFinal = greedy_distribution(users, servers, solGreedyFinal, 0, 0);
            solBBFinal = bb_distribution(solGreedyFinal, 0, 0, users, servers);
            printSolutionD(solBBFinal);
            Console.WriteLine("\tNombre de iteracions: " + numCridesRec);
            numCridesRec = 0;
        }


    }
}
