﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using static instasalle_p2.Functionalities;

namespace instasalle_p2
{
    class Menu
    {
        public int option1;
        public int option2;
        const int MIN_OPTION = 1, MAX_OPTION_MAIN = 4, MAX_OPTION_ALGO = 5;

        ArrayList arrayList = new ArrayList();

        public void printHeader()
        {
            Console.WriteLine("\n============= INSTASALLE =============");
        }
        public void printMainMenu()
        {
            Console.WriteLine("\nMain Menu:\n\n\t1. Availability\n\t2. Load distribution\n\t3. Read new file (Underconstruction)");
            Console.WriteLine("\n\t4. Exit\n\nChoose option: ");
        }

        public void printAlgoMenu()
        {
            Console.WriteLine("\n\tAlgorithms:");
            Console.WriteLine("\t\t1. Backtracking");
            Console.WriteLine("\t\t2. Branch & Bound");
            Console.WriteLine("\t\t3. Greedy");
            Console.WriteLine("\t\t4. Greedy + Backtracking");
            Console.WriteLine("\t\t5. Greedy + Branch & Bound");
            Console.WriteLine();
            Console.Write("\tChoose algorithm: ");
        }

        public void printErrOption()
        {
            Console.WriteLine("\nError, incorrect option.");
        }

        public void getOption(int type)
        {
            if (type == 1)
            {
                string str = Console.ReadLine();
                int.TryParse(str, out this.option1);
            }
            if (type == 2)
            {
                string str = Console.ReadLine();
                int.TryParse(str, out this.option2);
            }
            
        }

        public bool isCorrect(int min, int max, int type)
        {
            if (type == 1)
            {
                return this.option1 >= min && this.option1 <= max;
            }
            return this.option2 >= min && this.option2 <= max;
        }

        public bool isExit()
        {
            return this.option1 == 4;
        }

        public static void printFinalResults (SolutionPathA[] s, string name, int servDest_cost, int servDest_fiable)
        {
            Console.WriteLine("\n\tFinal Path " +  name + " with smallest cost is to server " + servDest_cost + ":");
            printPath(s[0]);
            Console.WriteLine("\tTotal cost of path: " + s[0].cost_total);
            Console.WriteLine("\n\tFinal Path " + name + " with best reliability is to server " + servDest_fiable + ":");
            printPath(s[1]);
            Console.WriteLine("\tTotal reliability of path: " + s[1].reliability_total);
        }

        public static SolutionPathA[] doAlgorithmForAllChildren (int option, Server servA, Server[] servB, Node[] node, SolutionPathA[] solFinal, ref int servDest_cost, ref int servDest_fiable)
        {
            int i;
            int num_servers = servB.Length;
            SolutionPathA[] solAct = new SolutionPathA[2];
            for (i = 0; i < num_servers; i++)
            {
                // per evitar buscar un camí a si mateix
                if (i == servA.id - 1)
                    continue;

              
                switch (option) {
                    case 1:
                        solAct = Functionalities.backtrackingA(node, servA, servB[i]);
                        break;

                    case 2:
                        solAct = Functionalities.branchAndBoundA(node, servA, servB[i]);
                        break;

                    case 3:
                        solAct = Functionalities.greedyA(node, servA, servB[i]);
                        break;

                    case 4:
                        solAct = Functionalities.greedyBacktrackingA(node, servA, servB[i]);
                        break;

                    case 5:
                        solAct = Functionalities.greedyBranchAndBoundA(node, servA, servB[i]);
                        break;
                }

                // Minimització del cost
                if (solAct[0].cost_total < solFinal[0].cost_total)
                {
                    solFinal[0] = solAct[0];
                    servDest_cost = servB[i].id;
                }

                //Maximització de la fiabilitat
                if (solAct[1].reliability_total > solFinal[1].reliability_total)
                {
                    solFinal[1] = solAct[1];
                    servDest_fiable = servB[i].id;
                }
            }
            
            // Si servDest = 0, vol dir que la primera solució és la millor
            if (servDest_cost == 0)
            {
                servDest_cost++;
            }
            if (servDest_fiable == 0)
            {
                servDest_fiable++;
            }
            return solFinal;
        }
        
        public static void switchAvailability(int option, Server servA, Server[] servB, Node[] node)
        {

            int servDest_cost = 0;
            int servDest_fiable = 0;
            SolutionPathA[] solBackTrackingActual, solBackTrackingFinal;
            SolutionPathA[] solBranchAndBoundActual, solBranchAndBoundFinal;
            SolutionPathA[] solGreedyActual, solGreedyFinal;
            SolutionPathA[] solGreedyBranchBoundActual, solGreedyBranchBoundFinal;
            SolutionPathA[] solGreedyBackTrackingActual, solGreedyBackTrackingFinal;
           
            Stopwatch stopWatch = new Stopwatch();
            TimeSpan ts = stopWatch.Elapsed;
            switch (option)
            {
                case 1:
                    stopWatch.Start();
                    solBackTrackingFinal = Functionalities.backtrackingA(node, servA, servB[0]);

                    // Comparem la solució trobada per al primer servidor i treiem la millor solució
                    solBackTrackingFinal = doAlgorithmForAllChildren(option, servA, servB, node, solBackTrackingFinal, ref servDest_cost, ref servDest_fiable);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;           
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    printFinalResults(solBackTrackingFinal, "BacktrackingA", servDest_cost, servDest_fiable);
                    break;
                case 2:
                    stopWatch.Start();
                    solBranchAndBoundFinal = Functionalities.branchAndBoundA(node, servA, servB[0]);

                    // Comparem la solució trobada per al primer servidor i treiem la millor solució
                    solBranchAndBoundFinal = doAlgorithmForAllChildren(option, servA, servB, node, solBranchAndBoundFinal, ref servDest_cost, ref servDest_fiable);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    printFinalResults(solBranchAndBoundFinal, "BranchAndBoundA", servDest_cost, servDest_fiable);
                    break;

                case 3:
                    stopWatch.Start();
                    solGreedyFinal = Functionalities.greedyA(node, servA, servB[0]);

                    // Comparem la solució trobada per al primer servidor i treiem la millor solució
                    solGreedyFinal = doAlgorithmForAllChildren(option, servA, servB, node, solGreedyFinal, ref servDest_cost, ref servDest_fiable);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    printFinalResults(solGreedyFinal, "GreedyA", servDest_cost, servDest_fiable);
                    break;

                case 4:
                    //Functionalities.greedyBacktrackingA(node, servA, servB);
                    stopWatch.Start();
                    solGreedyBackTrackingFinal = Functionalities.greedyBacktrackingA(node, servA, servB[0]);

                    // Comparem la solució trobada per al primer servidor i treiem la millor solució
                    solGreedyBackTrackingFinal = doAlgorithmForAllChildren(option, servA, servB, node, solGreedyBackTrackingFinal, ref servDest_cost, ref servDest_fiable);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    printFinalResults(solGreedyBackTrackingFinal, "GreedyBackTrackingA", servDest_cost, servDest_fiable);
                    break;

                case 5:

                    //Functionalities.greedyBranchAndBoundA(node, servA, servB);
                    stopWatch.Start();
                    solGreedyBranchBoundFinal = Functionalities.greedyBranchAndBoundA(node, servA, servB[0]);

                    // Comparem la solució trobada per al primer servidor i treiem la millor solució
                    solGreedyBranchBoundFinal = doAlgorithmForAllChildren(option, servA, servB, node, solGreedyBranchBoundFinal, ref servDest_cost, ref servDest_fiable);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    printFinalResults(solGreedyBranchBoundFinal, "GreedyBranchBoundA", servDest_cost, servDest_fiable);
                    break;
            }
        }

        public static void switchDistribution (int option, Usuari[] users, Server[] servers)
        {
            SolutionD solBackTrackingFinal = new SolutionD();
            SolutionD solBranchAndBoundActual, solBranchAndBoundFinal;
            SolutionD solGreedyActual, solGreedyFinal;
            SolutionD solGreedyBranchBoundActual, solGreedyBranchBoundFinal;

            Stopwatch stopWatch = new Stopwatch();
            TimeSpan ts = stopWatch.Elapsed;
            switch (option)
            {
                case 1:
                    stopWatch.Start();
                    solBackTrackingFinal = Functionalities.backtrackingD(users, servers);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    
                    break;
                case 2:
                    stopWatch.Start();
                    solBranchAndBoundFinal = Functionalities.branchAndBoundD(users, servers);

                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    break;
                case 3:
                    stopWatch.Start();
                    Functionalities.greedyD(users, servers);
                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    break;
                case 4:
                    stopWatch.Start();
                    Functionalities.greedyBacktrackingD(servers, users);
                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    break;
                case 5:
                    stopWatch.Start();
                    Functionalities.greedyBranchAndBoundD(servers, users);
                    stopWatch.Stop();
                    ts = stopWatch.Elapsed;
                    Console.WriteLine("\n\tTOTAL TIME: " + ts.TotalMilliseconds);
                    break;
            }
        }

       
        public void initUsers (Usuari[] u, ref Usuari A, ref Usuari B) 
        {
            string userA, userB;
            do
            {
                Console.WriteLine("\n\tAvailable users: ");
                foreach (Usuari usuari in u) { Console.WriteLine("\t" + usuari.username); }

                Console.Write("\n\tChoose user A: ");
                userA = Console.ReadLine();

                if (!Usuari.userExists(u,userA, ref A)) { Console.WriteLine("\tError! Type the correct name of the available user, please."); }

            } while (!Usuari.userExists(u, userA, ref A));
        }

        public void initServers (Server[] s, int idA, ref Server A, int idB, ref Server B)
        {
            A = s[idA - 1];
            //B = s[idB - 1];
        }

        public void startProgram (int functionality, int algorithm, ref ArrayList node, ref ArrayList server, ref ArrayList user)
        {
            //Converting arraylists to array of posts
            Usuari[] arrayUsers = user.ToArray(typeof(Usuari)) as Usuari[];
            Server[] arrayServer = server.ToArray(typeof(Server)) as Server[];
            Node[] arrayNode = node.ToArray(typeof(Node)) as Node[];

            List<GeoCoordinate> listServer = Server.convertMyGeoLocation(arrayServer);
          
            Usuari.getUserServer(ref arrayUsers, listServer);

            Usuari userA = new Usuari(); Usuari userB = new Usuari();
            Server servA = new Server(); Server servB = new Server();
           
            switch (functionality)
            {
                case 1: //availability
                    initUsers(arrayUsers, ref userA, ref userB);
                    initServers(arrayServer, userA.server, ref servA, userB.server, ref servB);
                    switchAvailability(algorithm, servA, arrayServer, arrayNode);
                    break;
                case 2: //distribution

                    switchDistribution(algorithm, arrayUsers, arrayServer);

                    break;

                case 3: //load new file NOT DONE IMPLEMENTING
                    File.loadNewFile(ref node, ref server, ref user);
                    break;
              
            }
        }
    }
}
