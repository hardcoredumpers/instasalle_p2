﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Device.Location;


namespace instasalle_p2
{
    class Usuari
    {
        public string username { get; set; }
        public long followers { get; set; }
        public long follows { get; set; }
        public double activity { get; set; }
        public Connection[] connections { get; set; }
        public Post[] posts { get; set; }
        public List<long> likedPosts { get; set; }
        public List<long> commentedPosts { get; set; }
        public int server { get; set; }

        struct UserServer
        {
            public int id;
            public double distance;
        }
        
        public static void getUserServer (ref Usuari[] u, List<GeoCoordinate> serverLoc)
        {
            GeoCoordinate userLoc = new GeoCoordinate();
            UserServer us;
            List<UserServer> userLocList = new List<UserServer>();
            foreach (Usuari user in u)
            {
                //get user location from last post
                userLoc = Server.ConvertToGeoLocation(user.posts[0].location[0], user.posts[0].location[1]);
                //get distance of this location for each existing server
                for (int i = 0; i < serverLoc.Count(); i++)
                {
                    us.id = i + 1;
                    us.distance = userLoc.GetDistanceTo(serverLoc[i]);
                    userLocList.Add(us);
                }
                //we get the closer server
                List <UserServer> newList = userLocList.OrderBy(o => o.distance).ToList();
                user.server = newList[0].id;
                userLocList.Clear();
                newList.Clear();
            }
        }

        public static bool userExists (Usuari [] u, string username, ref Usuari usuari)
        {
            bool ok = false;

            for (int i = 0; i < u.Count() && !ok; i++)
            {
                if (username.Equals(u[i].username)) { ok = true; usuari = u[i]; }
            }
            return ok;
        }
    }

}
