using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p2
{
    class ConnectionNode
    {
        public int to { get; set; }
        public string name { get; set; }
        public int cost { get; set; }
    }

}