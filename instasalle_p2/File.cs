﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace instasalle_p2
{
    class File
    {
        public static ArrayList loadNodeFile(String filename)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                StreamReader r = new StreamReader(filename);
                string json = r.ReadToEnd();
                var file = JsonConvert.DeserializeObject<List<Node>>(json);
                for (int i = 0; i < file.Count(); i++)
                {
                    arrayList.Add(file[i]);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error! File doesn't exist.");
            }
            return arrayList;

        }

        public static ArrayList loadServerFile(String filename)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                StreamReader r = new StreamReader(filename);
                string json = r.ReadToEnd();
                var file = JsonConvert.DeserializeObject<List<Server>>(json);
                for (int i = 0; i < file.Count(); i++)
                {
                    arrayList.Add(file[i]);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error! File doesn't exist.");
            }
            return arrayList;

        }

        public static ArrayList loadUserFile(String filename)
        {
            ArrayList arrayList = new ArrayList();
            try
            {
                StreamReader r = new StreamReader(filename);
                string json = r.ReadToEnd();
                var file = JsonConvert.DeserializeObject<List<Usuari>>(json);
                for (int i = 0; i < file.Count(); i++)
                {
                    arrayList.Add(file[i]);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Error! File doesn't exist.");
            }
            return arrayList;

        }

        public static void typeFile (String type, ref ArrayList node, ref ArrayList server, ref ArrayList user)
        {
            ArrayList arrNode = new ArrayList();
            ArrayList arrServer = new ArrayList();
            ArrayList arrUser = new ArrayList();

            if (type.Contains("node"))
            {
                node = loadNodeFile(type);
            }

            if (type.Contains("server"))
            {
                server = loadServerFile(type);
            }

            if (type.Contains("user"))
            {
                user = loadUserFile(type);
            }
        }

        public static void loadNewFile (ref ArrayList node, ref ArrayList server, ref ArrayList user)
        {
            Console.WriteLine("\nEnter existing new file name: ");
            String filename = Console.ReadLine();
            typeFile(filename+".json", ref node, ref server, ref user);
        }

    }
}
