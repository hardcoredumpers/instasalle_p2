using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Device.Location;

namespace instasalle_p2
{
    class Server
    {
        public int id { get; set; }
        public string country { get; set; }
        public double[] location { get; set; }
        public int[] reachable_from { get; set; }

       

        public static GeoCoordinate ConvertToGeoLocation (double lat, double lon)
        {
            return new GeoCoordinate(lat, lon);
        }

        public static List<GeoCoordinate> convertMyGeoLocation(Server[] s)
        {
            List<GeoCoordinate> serverLocation = new List<GeoCoordinate>();
            GeoCoordinate geoloc;
            foreach (Server server in s)
            {
                geoloc = ConvertToGeoLocation(server.location[0], server.location[1]);
                serverLocation.Add(geoloc);
            }
            return serverLocation;
        }
    }

}