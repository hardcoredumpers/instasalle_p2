using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace instasalle_p2
{
    class Node
    {
        public int id { get; set; }
        public double reliability { get; set; }
        public ConnectionNode[] connectsTo { get; set; }
    }

}
